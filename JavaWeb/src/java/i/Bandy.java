package i;

import Controlleur.ModelView;
import Controlleur.Path;
import java.util.HashMap;

public class Bandy {
    String truc ;
    String bla;

    public String getTruc() {
        return truc;
    }

    public void setTruc(String truc) {
        this.truc = truc;
    }

    public String getBla() {
        return bla;
    }

    public void setBla(String bla) {
        this.bla = bla;
    }
    
    @Path(url="manisabandy")
    public  ModelView manisaBandy(){
        ModelView m=new ModelView();
        m.addHmap("nombre", 5);
        m.addHmap("Phrase", "le temps est beau");
        m.addHmap("Bandy", new Bandy());
        System.out.println("bla : "+bla);
        System.out.println("truc : "+truc);
        m.setTarget("bla.html");
        return m;
    } 
//    @Path(url="mikotibandy")
    public ModelView mikotibandy(){
        ModelView mv=new ModelView();
        mv.setTarget("bla.html");
        return mv;
    }
}
